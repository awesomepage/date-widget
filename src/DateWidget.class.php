<?php

// Based on https://codex.wordpress.org/Widgets_API
namespace DateWidgetTest;


class DateWidget extends \WP_Widget
{
    public function __construct()
    {
        $this->registerScripts();

        $options = array(
            'classname'   => 'datepicker_widget_class',
            'description' => 'Datepicker Test',
        );

        parent::__construct('date_picker_test', __('Date Picker Test', 'date-widget-test'), $options);
    }


    private function registerScripts()
    {
        add_action('template_redirect', array( __CLASS__, 'enqueueStylesAndScripts' ));
        return $this;
    }


    public static function enqueueStylesAndScripts()
    {
        $base_url = untrailingslashit(plugin_dir_url(__FILE__)) . "/../resources/js/";

        wp_enqueue_style('jquery-ui-base-style', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
        wp_enqueue_script('jquery-ui-datepicker', false, [ 'jquery' ]);
        wp_enqueue_script('date-widget-test-js', $base_url . 'datepicker-widget-test.js', array( 'jquery', 'jquery-ui-datepicker' ), false);
        $demo            = new self();
        $widget_settings = array();
        foreach( $demo->get_settings() as $id => $settings ) {
            $widget_settings[] = array( 'id' => "date_picker_test-{$id}", 'dateFormat' => $settings['date_format'] );
        }
        wp_localize_script('date-widget-test-js', 'test_date_widget_settings', $widget_settings);

    }


    //WordPress Widget API Functions
    public function widget( $args, $instance )
    {
        $date_format = ( empty( $instance['date_format'] ) ? __('yy-mm-dd', 'date-widget-test') : trim($instance['date_format']) );
        echo $args['before_widget'];
        echo "{$args['before_title']}Codeable Datewidget Test{$args['after_title']}";
        ?>
        <p><?php _e('Choose a date', 'date-widget-test'); ?></p><p>
        <input type="text" name="date_widget_test" class="datepicker_widget_test"/>
    </p>
        <?php
        echo $args['after_widget'];
    }


    public function form( $instance )
    {
        $date_format = ( !empty( $instance['date_format'] ) ? $instance['date_format'] : _x('yy-mm-dd', 'Default date format', 'date-widget-test') );
        $field_id    = $this->get_field_id('date_format');
        ?>
        <p>
            <label for="<?php echo $field_id; ?>">
                <?php _e('Date format:', 'date-widget-test'); ?>
            </label>
            <input id="<?php echo $field_id; ?>" name="<?php echo $this->get_field_name('date_format'); ?>" type="text" value="<?php echo $date_format; ?>"/>
        </p>
        <?php
    }


    public function update( $newInstance, $prevInstance )
    {

        $instance = array(
            'date_format' => ( !empty( $newInstance['date_format'] ) ? strip_tags($newInstance['date_format']) : __('yy-mm-dd', 'date-widget-test') ),
        );

        return $instance;
    }

}