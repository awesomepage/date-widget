<?php


namespace DateWidgetTest;


class Setup
{
    public function __construct()
    {
        $this->registerTextDomain()->registerWidgets();
    }


    private function registerTextDomain()
    {
        $path = untrailingslashit(plugin_basename(__FILE__)) . "/../resources/languages/";

        load_plugin_textdomain('date-widget-test', false, $path);

        return $this;
    }


    public function registerWidgets()
    {
        register_widget('DateWidgetTest\\DateWidget');

        return $this;
    }

}