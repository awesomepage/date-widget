#Date Widget Plugin

This plugin creates a custom widget type called Date Field.

**Requirements**:

- The widget will contain a text input field with jQuery UI Datepicker binded to it. 
- The input will become a Datepicker field on the frontend. 
- The widget backend should have the ability for the user to change the date format that datepicker will insert into the input field.

##Change Log

- Removed method registerFilters from Setup class
- Implemented register widget with full class name
- Customized js to support different dateformats for different datepickers
- Added file resources/js/datepicker-widget-test.js to configure the widgets on the fornt end
- Added Widget methods to Date widget class
- Added Setup class 
- Added composer psr-4 autoload
- Implemented composer