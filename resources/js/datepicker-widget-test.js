;
jQuery(document).ready(function(){

    var dateFormat = 'yy-mm-dd';
    console.log( 'Type of wp object: ' + typeof test_date_widget_settings );
    if( 'object' === typeof test_date_widget_settings ){
        console.log('Setting custom dateformats');
        for( var i = 0; i < test_date_widget_settings.length; i++ ){
            console.log('Setting date format' + test_date_widget_settings[i].dateFormat + ' for datepicker within ' + test_date_widget_settings[i].id);
            jQuery('#'+test_date_widget_settings[i].id).find('input[type=text].datepicker_widget_test').datepicker({
                dateFormat: test_date_widget_settings[i].dateFormat
            });
        }
    } else {
        jQuery('input[type=text].datepicker_widget_test').datepicker({
            dateFormat: dateFormat
        });
    }

});