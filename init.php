<?php
/**
 * Plugin name: DateWidget Codeable Test
 * Plugin URI: https://bitbucket.org/awesomepage/date-widget
 * Description: A WordPress plugin to test my code abilities
 * Version: 1.0.0
 * Author: Page Carbajal
 * Author URI: http://pagecarbajal.com
 */

// Enable composer autoload
require_once('vendor/autoload.php');

function launchDateWidget()
{
    // Launch the plugin setup
    new \DateWidgetTest\Setup();
}
add_action('setup_theme', 'launchDateWidget');